import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { COLORS } from './src/Styles/Color'
import { NativeBaseProvider } from 'native-base';
// screen import 
import LoginPage from './src/screen/Auth/LoginPage';
import RegisterPage from './src/screen/Auth/RegisterPage';
import Dashboard from './src/screen/Main/Dashboard';
import Pokebag from './src/screen/Main/Pokebag';
import DetaiPokemon from './src/screen/Main/DetaiPokemon';
import { Provider } from 'react-redux/';
import store from './src/moduls/store'

const Stack = createNativeStackNavigator()

const App = () => {
  return (
  <Provider store={store}>
  <NativeBaseProvider>
    <NavigationContainer>
      <Stack.Navigator
          detachInactiveScreens={false}
          initialRouteName="Auth"
          screenOptions={{
            headerShown: false,
            cardStyle: { backgroundColor: COLORS.white },
          }}>
          <Stack.Screen name="Login" component={LoginPage} />
          <Stack.Screen name="Pokebag" component={Pokebag} />
          <Stack.Screen name="Register" component={RegisterPage} />
          <Stack.Screen name="Dashboard" component={Dashboard}/>     
          <Stack.Screen name="DetaiPokemon" component={DetaiPokemon}/>     
      </Stack.Navigator>   
    </NavigationContainer>
  </NativeBaseProvider>
  </Provider>
  )
}

export default App

const styles = StyleSheet.create({})
