import React, {useState, useEffect} from "react";
import {
    View,
    Text
} from 'native-base';
import {
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Alert
} from 'react-native';
import auth, { firebase } from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { Formik } from "formik";
import { COLORS } from '../../Styles/Color';

const Login = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const onLoginRDB = () => {
        try {
            database()
                .ref('users/')
                .orderByChild("emailId")
                .equalTo(email)
                .once('value')
                .then(async snapshot => {
                    if (snapshot.val() == null) {
                        Alert.alert("Invalid Email Id")
                        return false;
                    }
                    let userData = Object.values(snapshot.val())[0];
                    if (userData?.password != password) {
                        Alert.alert("Error", "Invalid Password!");
                        return false;
                    }
                    console.log('User data: ', userData);
                    navigation.navigate('Dashboard', { userData: userData })
                });
        } catch (error) {
            Alert.alert('Error', 'Not Found User')
        }

    }
    
    return (
        <Formik
            initialValues={{ email: '', password: ''}}
        >
        {({ handleChange, handleBlur, handleSubmit, values, setFieldTouched, touched, isValid, errors }) => (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View style={[styles.inputContainer, { marginTop: 10 }]}>
            <TextInput
                    style={styles.inputs}
                    placeholder="Enter Email Id"
                    // keyboardType="number-pad"
                    underlineColorAndroid="transparent"
                    onChangeText={value => {
                        setEmail(value)
                    }}
                    value={email}
                    onBlur={() => setFieldTouched('email')}
                />
            {touched.email && errors.email && <Text style={{ color: 'red' }}>{errors.email}</Text>}
            </View>
            <View style={[styles.inputContainer, { marginTop: 10 }]}>
                <TextInput
                    style={styles.inputs}
                    placeholder="Enter Password"
                    // keyboardType="number-pad"
                    underlineColorAndroid="transparent"
                    onChangeText={value => {
                        setPassword(value)
                    }}
                    value={password}
                    onBlur={() => setFieldTouched('password')}
                    secureTextEntry={true}
                />
            {touched.password && errors.password && <Text style={{ color: 'red' }}>{errors.password}</Text>}
            </View>
            <TouchableOpacity
                style={styles.btn}
                onPress={() => onLoginRDB()}
            >
                <Text style={styles.btnText}>Login Now</Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', margin: 10 }}>
                <Text>Not Have Account ?</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                    <Text>Register Here!!</Text>
                </TouchableOpacity>
            </View>

        </View>
        )}
        </Formik>
    )
}


export default Login;

const styles = StyleSheet.create({
    inputs: {
        borderBottomColor: COLORS.white,
        color: COLORS.black,
        paddingLeft: 10,
        flex: 1
    },
    inputContainer: {
        borderRadius: 30,
        height: 48,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        marginBottom: 10,
        elevation: 2,
        borderColor: COLORS.green,
        borderWidth: 2,
        width: '90%'
    },
    btnText: {
        color: '#fff',
        fontSize: 14,
        marginTop: 2,
    },
    btn: {
        backgroundColor: COLORS.theme,
        width: '90%',
        height: 50,
        borderRadius: 30,
        elevation: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})