import { Button } from "native-base";
import React, {useState, useEffect} from "react";
import {
    View,
    Text,
    FlatList,
    Platform,
    Alert,
    StyleSheet,
    TouchableOpacity,
    Image,
    Pressable
} from 'react-native';
import  { useSelector, useDispatch} from 'react-redux';
import { getPokemonList } from "../../moduls/mutation";

const Dashboard = ({navigation}) => {
    const dispatch = useDispatch();
    const pokemonList = useSelector(state => state.pokemonList);

    useEffect(() => {
        dispatch(getPokemonList({}));
    }, [])

      
    return (
        <View style={styles.container}>
            <Text style={styles.h1}>Pokedex</Text>
            <FlatList
            data = {pokemonList}
            renderItem = {({item}) =>
                    <View style={styles.GridViewContainer}>
                        <View style={styles.row}>
                            <View>
                                <Image source={{uri:'https://cdn-icons-png.flaticon.com/512/419/419467.png'}}/>
                            </View>
                            <View>
                                <Pressable onPress={() => {navigation.navigate("DetaiPokemon", {id:item.url.match(/(\d+)\/$/)[1]})}}>
                                <Text style={styles.GridViewTextLayout}>{item.name}</Text>
                                </Pressable>
                            </View>
                        </View>
                    </View>
            }
            numColumns={2}
            />
            <View>
              <Button onPress={()=>{navigation.navigate("Pokebag")}}>Pokebag</Button>
            </View>
        </View>
    )
}

export default Dashboard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",    
  },
  headerText: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    fontWeight: "bold"
  },
  GridViewContainer: {
   flex:1,
   justifyContent: 'center',
   alignItems: 'center',
   height: 40,
   margin: 5,
   backgroundColor: 'white',
   borderRadius: 10,
},
GridViewTextLayout: {
   fontSize: 15,
   fontWeight: 'bold',
   justifyContent: 'center',
   color: 'black',
   padding: 10,
 },
 row:{
  flexDirection: 'row',
  flexWrap: 'wrap',
 },
 h1: {
  fontSize: 25,
  fontWeight: "bold",
  color: "black",
  marginLeft: 10
 }
});