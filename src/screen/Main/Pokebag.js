import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import database from '@react-native-firebase/database';
import { Center, Row } from 'native-base';

const Pokebag = ({ navigation }) => {

    const [Data, setData] = useState([])

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        database()
            .ref('Bag/')
            .once('value')
            .then(snapshot => {
                // console.log('all User data: ', Object.values(snapshot.val()));
                // setData('all User data: ', Object.values(snapshot.val()));
                setData(Object.values(snapshot.val()))
            });
    };

    console.log(Data)
    const list = () => {
        return Data.map((element) => {
            return (
                <View style={{ margin: 10, backgroundColor: 'White'}}>
                    {/* <Text>{element.idPokemon}</Text> */}                    
                        <TouchableOpacity onPress={() => {
                            navigation.navigate('DetaiPokemon', {
                                id: `${element.idPokemon}`,
                            })
                        }}>                            
                            <View>
                                <Image
                                source={{ uri: `${element.photo}` }}
                                style={{ margin: 12, width: 50, height: 50, alignSelf: 'center' }}
                            />
                            </View>                            
                            <View style={{alignItems: 'center'}}>
                                <Text style={{fontWeight: 'bold', fontSize: 20}}>{element.name}</Text>
                            </View>
                        </TouchableOpacity>                    
                    {/* <Text>{element.photo}</Text> */}
                    {/* <Text>{element.subtitle}</Text> */}
                </View>
            );
        });
    };
    return (
        <View>
            <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 24, paddingLeft: 10, paddingTop: 10}}>Pokebag</Text>
            {list()}
        </View>
    )
}

export default Pokebag