import { configureStore } from "@reduxjs/toolkit";
import * as api from "./api";

const initialState = {
    pokemonList: null,
    pokemonDetail: null,
}

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case "POKEMON_LIST":
            return {
                ...state,
                pokemonList: action.payload,
            };
        case "POKEMON_DETAIL":
            return {
                ...state,
                pokemonDetail: action.payload,
            };
        default:
            return state;
        }
}

const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware => 
        getDefaultMiddleware({
            thunk: {
                extraArgument: api,
            }
        })
})

export default store; 