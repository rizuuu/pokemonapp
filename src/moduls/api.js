import axios from "axios";

const pokemonListUrl = "https://pokeapi.co/api/v2/pokemon" 
const pokemonDetailUrl = "https://pokeapi.co/api/v2/pokemon/"

export const pokemonList = async (dispatch) => {
    const response = await axios.get(pokemonListUrl);
    dispatch({
        type: "POKEMON_LIST",
        payload: response.data.results
        });
}

export const pokemonDetail = async (dispatch, data) => {
    const response = await axios.get(pokemonDetailUrl + data.id);
    dispatch({
        type: "POKEMON_DETAIL",
        payload: response.data
    })
}